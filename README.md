# mPulse Mobile Coding Challenge

Skip to the [Running the Application](#running-the-application) section if you want to see the application in action. If not, feel free to read through the document for my analysis.

## Running the Application

### Docker

To run the application using Docker, you'll need both docker and docker-compose. Once those have been installed, you should be able to run the application with:

```
$ docker-compose up --build
```

The first time this command is ran it may take some time as it has to pull in the Redis, Postgres, and Python images as well as build the containers.

The docker-compose file describes the following services:
* `redis` - Container for running Redis
* `postgres` - Container for running PostgresQL
* `api` - Container for running the web server (exposes port 8000)
* `worker` - Container for running workers to process jobs on the job queue

When the API starts it will first run migrations `./manage.py migrate` and then run the server `./manage.py runserver 0.0.0.0:8000`. After all the services have successfully started, you can use the `load_members_with_duplicate_records.sh` script to load the initial members with duplicate data. The script will create a `POST` request to the `/api/members/upload` endpoint.

```
./load_members_with_duplicate_records.sh
```

After that you can then curl or open your web browser to: http://localhost:8000/api/members to see that all the members have been loaded. Since this is a small CSV file, you may not notice much in the console in terms of processing. Once that data has been loaded, you can then run:

```
./load_members.sh
```

This will load the data from the `member_data.csv`. Since this script contains 1,000 rows and the chunksize is set to 10, you should see significant console output. Once all data has been processed, you can use the same endpoint as above to see those users. You can also choose an `account_id` and verify that the users being all belong to that account. See the [endpoints](#endpoints) section for details about the API.

### Natively

#### Prerequisites
To run the application you'll need to have the following installed:
* Redis v5.0
* PostgreSQL 12
* Python 3.5+

If you wish to run the application natively, I recommend using a virtual env. My favorite tool for easily creating and using virtual envs is [virtualenvwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/). Once you've activated your virtualenv, you can install the requirments using:

```
$ pip install -r requirements.txt
```

You may want to check in the `mpulse/settings.py` to update the Redis connection information in `RQ_QUEUES` and the PostgresQL connection information in `DATABASES`.

Once that has been configured correctly, you'll need to run migrations:

```
$ ./manage.py migrate
```

Finally you can start the app using:

```
$ ./manage.py runserver
```


## Testing the Application

Similar to running the application, a docker-compose file (`docker-compose.test.yml`) has been provided to run tests. Tests are defined for `views` and `services`. To run the tests, simply run:

```
$ docker-compose -f docker-compose.test.yml run api
```

When running tests you should see an output similar to:

```
Starting mpulse_postgres_1 ... done
Starting mpulse_redis_1    ... done
Creating test database for alias 'default'...
System check identified no issues (0 silenced).
...............
----------------------------------------------------------------------
Ran 15 tests in 2.718s

OK
Destroying test database for alias 'default'...
```

## Business Requirements

Just as a refresher, the coding-challenge was to build a web service that stores data on medical members. Members must contain the following information:
* First Name
* Last Name
* Phone Number
* Client Member ID (equivalent to a Medical Record Number)
* Account ID

Multiple members can have the same `phone_number` or `client_member_id`, but these values must be unique to an `account_id`.

The web service should provide the following endpoints:
* Fetch all members for a given account
* Fetch a single member by their:
  * ID
  * Phone Number
  * Client Member ID
* Create a new member

In addition the service should also expose an endpoint that allows users to upload a CSV that will insert a new member for each row. Since these files can be large, the endpoint should not block the request nor the server. The endpoint should also efficiently be able to insert a large number of rows in the database.

## Application Requirements

This application requires Python 3.5+ along with PostgreSQL and Redis.

## Data Schema

As stated above Members can have the following information:
* ID
* First Name
* Last Name
* Phone Number
* Client Member ID (equivalent to a Medical Record Number)
* Account ID

Members are represented in the database under the `members_member` table, which has the following schema:

```
mpulse=# \d+ members_member;
                                                             Table "public.members_member"
      Column      |          Type          | Collation | Nullable |                  Default                   | Storage  | St
ats target | Description 
------------------+------------------------+-----------+----------+--------------------------------------------+----------+---
-----------+-------------
 id               | integer                |           | not null | nextval('members_member_id_seq'::regclass) | plain    |   
           | 
 first_name       | character varying(255) |           | not null |                                            | extended |   
           | 
 last_name        | character varying(255) |           | not null |                                            | extended |   
           | 
 phone_number     | character varying(10)  |           | not null |                                            | extended |   
           | 
 client_member_id | character varying(7)   |           | not null |                                            | extended |   
           | 
 account_id       | integer                |           | not null |                                            | plain    |   
           | 
Indexes:
    "members_member_pkey" PRIMARY KEY, btree (id)
    "members_member_account_id_client_member_id_74c4d746_uniq" UNIQUE CONSTRAINT, btree (account_id, client_member_id)
    "members_member_account_id_phone_number_424ef1b7_uniq" UNIQUE CONSTRAINT, btree (account_id, phone_number)
```

As part of the business requirements, we need to be able to model multiple members that have the same `phone_number` or `client_member_id` but these must be unique to an `account_id`. To ensure this, we create two unique indexes, one on `(account_id, client_member_id)` and `(account_id, phone_number)`. These indices can be seen above with the following names:
* `members_member_account_id_client_member_id_74c4d746_uniq`
* `members_member_account_id_phone_number_424ef1b7_uniq`

## Application Structure

The application is a Django web app using Django Rest Framework and Django Redis Queue. Much of the application is a standard CRUD RESTful API. The application exposes 6 endpoints, 5 of which are typical CRUD based end-points. PostgresQL was chosen as the database for persisting data. Redis was chosen as the message queue for offloading long-running tasks. At a high level, you can picture the infrastructure like this:

```
                   +--------------+
                   |              |
       +---------->+              |
       |           |   Postgres   |
+------+--- +      |              |
|           |      |              |
|           |      +--------------+
|    API    |
|           |
|           |      +--------------+
+-----+-----+      |              |
      |            |              |
      |            |     Redis    |
      +----------->+              |
                   +--------------+

```

### File Structure

While the application is a fairly typical Django app, certain modules have been created which may deviate from other projects.

#### Repo

```
.
├── docker-compose.test.yml # docker-compose file used for testing
├── docker-compose.yml # docker-compose file used for running application
├── Dockerfile # Dockerfile used to build application
├── entrypoint.sh # script used to migrate and run application in docker
├── images
├── load_members.sh # Script issue a cURL command to load data from `member_data.csv`
├── load_members_with_duplicate_records.sh  # Script issue a cURL command to load data from `member_data_with_duplicate_records.csv`
├── manage.py
├── media # Folder used to store uploaded files (MEDIA_ROOT)
├── member_data.csv # CSV file provided with coding challenge
├── member_data_with_duplicate_records.csv # CSV file provided with coding challenge
├── members # Member API Application
├── mpulse # Django application
├── README.md # The file you're reading ;P
└── requirements.txt
```

#### Members App
```
.
├── admin.py
├── apps.py
├── fixtures
│   └── members.json # Fixture for loading initial data used by tests.
├── __init__.py
├── migrations
│   ├── 0001_initial.py
│   ├── 0002_auto_20200207_0341.py
│   ├── 0003_auto_20200207_0343.py
│   ├── __init__.py
├── models.py
├── serializers.py # Serializers defined as part of DRF for models
├── services.py # Service classes used to interface with the database
├── tests.py
├── urls.py
├── views.py
└── workers.py # Worker functions used to process jobs from the Redis queue.
```

## Endpoints

### `GET /api/members/?account_id=$ACCOUNT_ID`

To meet the business requirement of fetching all users for a given Account ID, the API exposes an endpoint on `/api/members/` which accepts an optional query parameter `account_id`. If `account_id` is omitted, this endpoint simply returns all members. When provided, `account_id` will be used to fetch those members for the given `account_id`. If multiple `account_id` values are provided (ex: `?account_id=1&account_id=2`) only the first one will be used and the rest will be ignored.

### `GET /api/members/<int:pk>`

The API exposes an endpoint to fetch members by a specified member id.

### `GET /api/members/phone-number/<str:phone_number>`

This endpoint provides the ability to fetch a user by a phone number. Since multiple members can have the same phone number, this endpoint will raise an error if multiple users are returned for the given phone number.


### `GET /api/members/client-member/<str:client_member_id>`

This endpoint provides the ability to fetch a user by a Client Member ID. Since multiple members can have the same Client Member ID, this endpoint will raise an error if multiple users are returned for the given Client Member ID.

### `POST /api/members/`

This endpoint provides the ability to create a new member. The JSON request to create a new member must contain the following keys:
* `first_name` (str)
* `last_name` (str)
* `client_member_id` (int)
* `phone_number` (str)
* `account_id` (int)

If any of the following keys are missing or have the incorrect type the endpoint will return a `400 BAD REQUEST` explaining which values were missing/incorrect. On success, this endpoint returns a `201 CREATED`.

### `PUT /api/members/upload/<str:filename>`

This endpoint is the most interesting. The endpoint accepts a CSV file that will be processed to insert new members into the database. Since we're expecting that users may upload large CSV files, this endpoint itself does not process the CSV file. Instead, the endpoint enqueues a job into the `default` queue for the `enqueue_batch_jobs` worker.

The `enqueue_batch_jobs` worker takes the given file and splits the file up into chunks with 10 rows each (note: `chunksize=10` was chosen to emulate what would happen with larger CSV files. Given that the CSV only contained 1,000 rows, I wanted to make sure that this worker was working correctly). For each chunk, a new is created and a job is scheduled for that file with the `batch_insert_members` worker.


The `batch_insert_members` worker is responsible for inserting the members found in reads the given file into the database. The worker takes on a best-effort approach by trying to insert as many members as possible. The worker generates a list of members from the rows which are then inserted using a `bulk_create` statement. `bulk_create` will emit a single query to insert all the rows into the database. If conflicts arise for any of the members, those members will be skipped.

A high level overview of this scenario is modeled like so:


```
                                   +----------+       +-----+   +-----+
                                   |          |       |     |   |     +------------------+
                          +-------->   CSV    |       | CSV |   | CSV |                  |
                          |        |          |       +-^---+   +--^--+                  |
                          |        +-------+--+         |          |                     |
                          |                |            |          |                     |                +-------------+
                          |                |            |          |                     v                |             |
+---------+         +-----+-----+       +--v------------+----------+--------+       +----+---+            |             |
|         |         |           |       |                                   |       | Job:   |            |             |
|  user   +---PUT--->    API    |       |     Job: Enqueue Batch Jobs       |       | Batch  +----------->+   Database  |
|         |         |           |       |                                   |       | Insert |            |             |
+---------+         +----+------+       +---^-------------+-----------------+       +---^----+            |             |
                         |                  |             |                             |                 |             |
                  enqueue_batch_job         |             v                             |                 |             |
                         |      +-----------+-------------+-----+                       |                 +-------------+
                         |      |                               |                       |
                         |      |          REDIS QUEUE          +-----------------------+
                         +----->+                               |
                                +-------------------------------+
```


## Improvements

I always think it's good to reflect after these exercises to consider how building this within a business environment and rolling it out to production would change the project. My first thoughts are around the choice of using Redis as the Message Queue. In a production environment, I'm assuming we'd most likely be on some sort of public cloud provider (AWS, GCP, Azure, etc). In that case, I would probably opt to choose something like SQS or Kinesis (depending on performance/cost/business requirements) as a managed solution would require less maintenance and deployment. Another choice, is to simply use a managed Redis service like Elasticache.

When it comes to storage, I'm currently storing CSV files to the file system. This could create a number of issues depending on how the file system is mounted. For example, if these were containers or even VMs, this wouldn't be the best choice as destroying a container or VM would mean a loss of data. Instead, I would opt to store files to a storage service like S3. This has implications on the code as well. In the view responsible for the CSV upload, I'm simply using a `shutil.copy`. This obviously wouldn't be the case if we were using S3. To make the code more robust, we'd have to create a service that would abstract the details of the underlying file system away and make this configurable based upon the environment. For example, in development, unless working specifically on the S3 service, we'd want to store the files to the filesystem. In production, we'd like to be able to specify our S3 service to store files to S3.

Using something like S3 also gives us the ability to remove a portion of the infrastructure entirely. As mentioned earlier, we could replace Redis as the message queue in this project with something like SQS or Kinesis. If we moved the worker functions to a FaaS like AWS Lambda, we could completely remove a message queue and simply trigger a Lambda function when a PUT occurs on the S3 bucket. This would greatly reduce the maintenance and amount of infrastructure required. Although, these abstractions due come at a cost. Depending on the workload, it could be more expensive and require a more complex setup. While AWS Lambda itself is great, the deployment, configuration, and tuning aspect of it can definitely be tricky.

Finally, this API is completely exposed with no permissions, authentication, nor authorization. I imagine when it comes to sensitive information that we'd be storing. This would be a huge and quite troublesome vulnerability.
