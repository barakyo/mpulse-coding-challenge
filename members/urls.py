from django.urls import path

from . import views

urlpatterns = [
    path('', views.MemberList.as_view()),
    path('phone-number/<str:phone_number>',
         views.MemberByPhoneNumberDetailView.as_view()),
    path('client-member/<str:client_member_id>',
         views.MemberByClientMemberIdDetailView.as_view()),
    path('<int:pk>', views.MemberDetail.as_view()),
    path('upload/<str:filename>', views.FileUploadView.as_view())
]
