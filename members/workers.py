from django_rq import job
import csv
import django_rq
import pandas as pd

from members.models import Member

queue = django_rq.get_queue('default')


@job
def enqueue_batch_jobs(filepath: str) -> bool:
    """
    Worker function to split up large CSV files and enqueue jobs for the
    smaller chunked files.

    Args:
    filepath (str): The large csv file to split into chunks.
    """
    reader = pd.read_csv(filepath, iterator=True, chunksize=10)
    for index, chunk in enumerate(reader):
        chunked_file_path = f'{filepath}_{index}'
        chunk.to_csv(chunked_file_path)
        django_rq.enqueue(batch_insert_members, chunked_file_path)
    return True


@job
def batch_insert_members(filepath):
    """
    Worker function that accepts a CSV file and generates a list of members to
    bulk_create.

    Args:
    filepath (str): The csv file to process for inserting members.

    The CSV File must contain the following columns:
    * account_id
    * first_name
    * last_name
    * client_member_id
    * phone_number
    """
    members = []
    with open(filepath) as csv_file:
        reader = csv.DictReader(csv_file)
        for row in reader:
            member = Member(account_id=row['account_id'],
                            first_name=row['first_name'],
                            last_name=row['last_name'],
                            client_member_id=row['client_member_id'],
                            phone_number=row['phone_number'])
            members.append(member)
    Member.objects.bulk_create(members, ignore_conflicts=True)
    return True
