from glob import glob
from django.conf import settings
from django.test import TestCase
from rest_framework.test import APITestCase

import django_rq
import os

from members.services import MemberService
from members.workers import enqueue_batch_jobs


class MemberServiceTestCase(TestCase):
    fixtures = ['members.json']

    def test_all_returns_all_members(self):
        self.assertEqual(len(MemberService.all()), 1000)

    def test_all_filter_by_account_id(self):
        members = MemberService.all(account_id='5')
        for member in members:
            self.assertEqual(member.account_id, 5)

    def test_get_by_id(self):
        member = MemberService.get(1)
        self.assertEqual(member.id, 1)

    def test_get_by_phone_number(self):
        phone_number = "1284628753"
        member = MemberService.get_by_phone_number(phone_number)
        self.assertEqual(member.phone_number, phone_number)

    def test_get_by_client_member_id(self):
        client_member_id = "7228138"
        member = MemberService.get_by_client_member_id(client_member_id)
        self.assertEqual(member.client_member_id, client_member_id)


class MemberViewTestCase(APITestCase):
    fixtures = ['members.json']

    def test_get_members_by_account_id(self):
        """Tests that we can retrieve members by a given account_id"""
        response = self.client.get('/api/members/', {'account_id': '5'})
        self.assertEqual(response.status_code, 200)
        members = response.data
        for member in members:
            self.assertEqual(member['account_id'], 5)

    def test_get_members_by_first_account_id(self):
        """
        Tests that the route returns only members only for the first
        acccount_id
        """
        response = self.client.get('/api/members/', {'account_id': ['5', '3']})
        self.assertEqual(response.status_code, 200)
        members = response.data
        for member in members:
            self.assertEqual(member['account_id'], 5)

    def test_get_member_by_id(self):
        """Tests that we can retrieve members by their id"""
        response = self.client.get('/api/members/1')
        self.assertEqual(response.status_code, 200)
        member = response.data
        self.assertEqual(member['id'], 1)

    def test_get_member_by_raises_with_invalid_id(self):
        """Verifies that we raise an Http404 for an invalid member id"""
        response = self.client.get('/api/members/0')
        self.assertEqual(response.status_code, 404)

    def test_get_member_by_phone_number(self):
        """Tests that we can retrieve members by their phone number"""
        phone_number = "1284628753"
        response = self.client.get(
            f'/api/members/phone-number/{ phone_number }')
        self.assertEqual(response.status_code, 200)
        member = response.data
        self.assertEqual(member['phone_number'], phone_number)

    def test_get_member_by_phone_number_raises_for_invalid_phone_number(self):
        """Verifies that we raise an Http404 for an invalid phone number"""
        phone_number = "0"
        response = self.client.get(
            f'/api/members/phone-number/{ phone_number }')
        self.assertEqual(response.status_code, 404)

    def test_get_member_by_client_member_id(self):
        """Tests that we can retrieve members by their client_member_id"""
        client_member_id = "7228138"
        response = self.client.get(
            f'/api/members/client-member/{ client_member_id }')
        self.assertEqual(response.status_code, 200)
        member = response.data
        self.assertEqual(member['client_member_id'], client_member_id)

    def test_get_member_by_client_member_id_raises_for_invalid_id(self):
        """Verifies that we raise an Http404 for an invalid client_member_id"""
        client_member_id = "0"
        response = self.client.get(
            f'/api/members/client-member/{ client_member_id }')
        self.assertEqual(response.status_code, 404)

    def test_create_member(self):
        """Verifies that we can successfully create a member with valid data"""
        data = {
            'client_member_id': '1337',
            'phone_number': '8188451059',
            'first_name': 'foo',
            'last_name': 'bar',
            'account_id': 5
        }
        response = self.client.post('/api/members/', data)
        self.assertEqual(response.status_code, 201)
        new_member_id = response.data["id"]
        member = MemberService.get(new_member_id)
        self.assertEqual(data['client_member_id'], member.client_member_id)
        self.assertEqual(data['phone_number'], member.phone_number)
        self.assertEqual(data['first_name'], member.first_name)
        self.assertEqual(data['last_name'], member.last_name)
        self.assertEqual(data['account_id'], member.account_id)

    def test_create_invalid_member(self):
        """Verifies that we all parameters are required when creating a member"""
        data = {
            'phone_number': '8188451059',
            'first_name': 'foo',
            'last_name': 'bar',
            'account_id': 5
        }
        response = self.client.post('/api/members/', data)
        self.assertEqual(response.status_code, 400)
        expected_error = ['This field is required.']
        self.assertEqual(response.data['client_member_id'], expected_error)


class MemberWorkerTestCase(TestCase):
    def setUp(self):
        print(os.getcwd())
        self.fixture_path = f'{os.getcwd()}/members/fixtures'
        self.csv_file = f'{self.fixture_path}/test.csv'
        self.queue = django_rq.get_queue('default')

    def test_enqueue_batch_jobs(self):
        """Verifies that the enqueue_batch_jobs creates the correct files"""
        self.queue.enqueue(enqueue_batch_jobs, self.csv_file)
        # Process Job
        django_rq.get_worker().work(burst=True)
        # There should now exist 5 csv files
        files = glob(f"{self.fixture_path}/test.csv_*")
        self.assertEqual(len(files), 5)

    def test_batch_insert_members(self):
        """Verifies that the enqueue_batch_jobs creates the correct files"""
        self.assertEqual(len(MemberService.all()), 0)
        self.queue.enqueue(enqueue_batch_jobs, self.csv_file)
        # Process Job
        django_rq.get_worker().work(burst=True)
        self.assertEqual(len(MemberService.all()), 46)

    def tearDown(self):
        files = glob(f"{self.fixture_path}/test.csv_*")
        for test_file in files:
            os.remove(test_file)
