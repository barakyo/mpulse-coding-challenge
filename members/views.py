import shutil
from time import time
from django.http import Http404
from django.conf import settings
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import FileUploadParser
import django_rq

from members.models import Member
from members.serializers import MemberSerializer
from members.services import MemberService
from members.workers import enqueue_batch_jobs


class MemberList(APIView):
    def get(self, request, format=None):
        """
        url: /api/members/

        Query Parameters:
            account_id (str): An optional account id to filter members by.

        Returns:
        A list of members optionally filtered by the provided account_id.
        """
        account_ids = request.query_params.getlist('account_id')
        members = []
        if len(account_ids) > 0:
            account_id = account_ids[0]
            members = MemberService.all(account_id)
        else:
            members = MemberService.all()
        serializer = MemberSerializer(members, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        """
        url: /api/members/

        Response Body:
        The response body must include the following values:
            account_id (str): Member's Account ID
            client_member_id (str): Member's Client Member ID
            first_name (str): The Member's first name
            last_name (str): The Member's last name
            phone_number (str): The Member's phone number

        Returns:
        201 - A newly created member
        400 - Error for why this member couldn't be created
        """
        serializer = MemberSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)

        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class MemberDetail(APIView):
    def get(self, request, pk, format=None):
        """
        url: /api/members/<int:pk>

        URL Parameters:
            pk (int): The member's ID for which you want to retrieve.

        Returns:
            200 - OK - If a member is found with that ID
            404 - Not Found - for an invalid member ID
        """
        try:
            member = MemberService.get(pk)
            serializer = MemberSerializer(member)
            return Response(serializer.data)
        except Member.DoesNotExist:
            raise Http404


class MemberByPhoneNumberDetailView(APIView):
    def get(self, request, phone_number, format=None):
        """
        url: /api/members/phone-number/<str:phone_number>

        URL Parameters:
            phone_number (str): The member's phone_number for which you want
            to retrieve.

        Returns:
            200 - OK - If a member is found with the given phone_number
            404 - Not Found - if no member is found with that phone_number
        """
        try:
            member = MemberService.get_by_phone_number(phone_number)
            serializer = MemberSerializer(member)
            return Response(serializer.data)
        except Member.DoesNotExist:
            raise Http404


class MemberByClientMemberIdDetailView(APIView):
    def get(self, request, client_member_id, format=None):
        """
        url: /api/members/client-member/<str:client_member_id>

        URL Parameters:
            client_member_id (str): The member's client_member_id

        Returns:
            200 - OK - If a member is found with the given client_member_id
            404 - Not Found - if no member is found with that client_member_id
        """
        try:
            member = MemberService.get_by_client_member_id(client_member_id)
            serializer = MemberSerializer(member)
            return Response(serializer.data)
        except Member.DoesNotExist:
            raise Http404


class FileUploadView(APIView):
    parser_classes = [FileUploadParser]
    queue = django_rq.get_queue('default')

    def put(self, request, filename, format=None):
        uploaded_file = request.FILES['file']
        if uploaded_file:
            timestamp = int(time())
            new_path = f'{settings.MEDIA_ROOT}/{uploaded_file}_{timestamp}'
            # Move file to a permanent location
            shutil.copy(uploaded_file.temporary_file_path(), new_path)
            # Enqueue a job to split this file into batches
            self.queue.enqueue(enqueue_batch_jobs, new_path)
        return Response(status=204)
