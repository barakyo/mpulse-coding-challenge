from django.db import models


# Create your models here.
# Example Member: (1, Yank, Deboo, 1284628753, 9436555, 12)
class Member(models.Model):
    first_name = models.CharField(max_length=255, null=False)
    last_name = models.CharField(max_length=255, null=False)
    phone_number = models.CharField(max_length=10, null=False)
    client_member_id = models.CharField(max_length=7, null=False)
    account_id = models.IntegerField(null=False)

    class Meta:
        unique_together = [
            ['account_id', 'phone_number'],
            ['account_id', 'client_member_id'],
        ]
