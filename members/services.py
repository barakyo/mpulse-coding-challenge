from typing import List
from members.models import Member


class MemberService:
    """
    Service class to house Member query functions.
    """
    def all(account_id: str = None) -> List[Member]:
        """
        Queries for all Members.

        Args:
            account_id (str): An optional account id to filter by. Defaults to
            None.

        Returns:
            A list of all members optional for the given account_id.
        """
        query = Member.objects
        if account_id:
            query = query.filter(account_id=account_id)
        return query.all()

    def get(id: int) -> Member:
        """
        Queries for a Member with the given id.

        Args:
            id (int): Member ID to query for

        Returns:
            A Member for the given id.
        """
        return Member.objects.get(pk=id)

    def get_by_phone_number(phone_number: str) -> Member:
        """
        Queries for a Member with the given phone_number.

        Args:
            phone_number (str): Member phone number as a string

        Returns:
            A Member with the given phone_number
        """
        return Member.objects.get(phone_number=phone_number)

    def get_by_client_member_id(client_member_id: str) -> Member:
        """
        Queries for a Member with the client_member_id.

        Args:
            client_member_id (int): Client Member ID

        Returns:
            A Member with the given client_member_id.
        """
        return Member.objects.get(client_member_id=client_member_id)
