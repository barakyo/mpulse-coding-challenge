#! /bin/bash

curl --location --request PUT 'http://localhost:8000/api/members/upload/member_data_with_duplicate_records.csv' \
     --header 'Content-Type: text/csv' \
     --data-binary '@member_data.csv'
