FROM python:3.8-buster

RUN mkdir /mpulse

WORKDIR /mpulse

COPY . .

RUN pip install -r requirements.txt

EXPOSE 8000

ENTRYPOINT ./manage.py runserver
